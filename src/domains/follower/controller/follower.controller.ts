import {Request, Response, Router} from 'express';
import HttpStatus from 'http-status';
import "express-async-errors";

import {db} from '@utils';

import {FollowerRepositoryImpl} from '../repository';
import {FollowerService, FollowerServiceImpl} from '../service';


export const followerRouter = Router();

// Use dependency injection
const service: FollowerService = new FollowerServiceImpl(new FollowerRepositoryImpl(db));

followerRouter.post('/follow/:followedId', async (req: Request, res: Response) => {
  const { userId } = res.locals.context;
  const { followedId } = req.params;

  await service.follow(userId, followedId);

  return res.status(HttpStatus.OK);
});

followerRouter.post('/unfollow/:followedId', async (req: Request, res: Response) => {
  const { userId } = res.locals.context;
  const { followedId: followedId } = req.params;

  await service.unfollow(userId, followedId);

  return res.status(HttpStatus.OK);
});
