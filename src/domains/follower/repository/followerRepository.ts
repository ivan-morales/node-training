export interface FollowerRepository {
  follow(userId: string, followedId: string): Promise<void>;
  unfollow(userId: string, followedId: string): Promise<void>;

}
