import {Prisma, PrismaClient} from '@prisma/client';

import {FollowerRepository} from '.';

export class FollowerRepositoryImpl implements FollowerRepository {
  constructor(private readonly db: PrismaClient) {
  }

  async follow(userId: string, followedId: string): Promise<void> {
    await this.db.follow.create({
      data: {
        followerId: userId,
        followedId: followedId,
      }
    }).catch((err) => {
      if (err instanceof Prisma.PrismaClientKnownRequestError) {
        if (err.code === 'P2002') {
          throw new Error('There is a unique constraint violation, the user is already following the other user');
        }
      } else throw err;
    });
  }

  async unfollow(userId: string, followedId: string): Promise<void> {
    await this.db.follow.delete({
      where: {
        followerId_followedId: { followerId: userId, followedId: followedId }
      }
    });
  }

}
