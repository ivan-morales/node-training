import {FollowerRepository} from '../repository';
import {FollowerService} from '.';

export class FollowerServiceImpl implements FollowerService {
  constructor(private readonly repository: FollowerRepository) {}

  async follow(userId: string, followedId: string): Promise<void> {
    return this.repository.follow(userId, followedId);
  }

  async unfollow(userId: string, followedId: string): Promise<void> {
    return this.repository.unfollow(userId, followedId);
  }

}
