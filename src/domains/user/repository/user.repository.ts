import { SignupInputDTO } from '@domains/auth/dto';
import { OffsetPagination } from '@types';
import { ExtendedUserDTO, UserDTO } from '../dto';

export interface UserRepository {
  create(data: SignupInputDTO): Promise<UserDTO>;
  getRecommendedUsersPaginated(options: OffsetPagination): Promise<UserDTO[]>;
  getById(userId: string): Promise<UserDTO | null>;
  getByEmailOrUsername(email?: string, username?: string): Promise<ExtendedUserDTO | null>;
  togglePrivateProfile(userId: string): Promise<void>;
  delete(userId: string): Promise<void>;
}
